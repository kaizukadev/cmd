<?php
    class Evento {
        public $id;
        public $nome;
        public $dtIni;
        public $dtFim;
        public $etapa01;
        public $etapa02;
        public $etapa03;
        public $coordenador;
        public $jurado01;
        public $jurado02;
        public $jurado03;
        public $editar;
        public $votar;
        public $ts_new;
        public $ts_upd;

        function __construct($id, $nome, $dtIni, $dtFim, $etapa01, $etapa02, $etapa03, 
                            $coordenador, $jurado01, $jurado02, $jurado03, $editar, $votar, 
                            $ts_new, $ts_upd){
            $this->id = $id;
            $this->nome = $nome;
            $this->dtIni = $dtIni;
            $this->dtFim = $dtFim;
            $this->etapa01 = $etapa01;
            $this->etapa02 = $etapa02;
            $this->etapa03 = $etapa03;
            $this->coordenador = $coordenador;
            $this->jurado01 = $jurado01;
            $this->jurado02 = $jurado02;
            $this->jurado03 = $jurado03;
            $this->editar = $editar;
            $this->votar = $votar;
            $this->ts_new = $ts_new;
            $this->ts_upd = $ts_upd;
        }
    }
?>