class ParticipanteHttpService{
    constructor(){}    
    
    
    enviarParticipante(participante, ok, error){
        
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 201) {
                ok();
            }
            else if(this.status !== 201){
                error(this.status);
            }

        };
        xhttp.open("POST", "http://localhost:8080/api/participantes", true);
        xhttp.setRequestHeader("Content-Type","application/json")
        xhttp.send(JSON.stringify(participante));
    }

    buscarParticipantes(ok,error) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                ok(JSON.parse(this.responseText));          
            }
            else if(this.status !== 200){
                error(this.status);
            }
        };
        xhttp.open("GET", "http://localhost:8080/api/participantes", true);
        xhttp.send();
    }

     deletarParticipante(id,ok,error) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                ok(JSON.parse(this.responseText));          
            }
            else if(this.status !== 200){
                error(this.status);
            }
        };
        xhttp.open("DELETE", "http://localhost:8080/api/participantes/"+id, true);
        xhttp.send();
    }

    buscarParticipante(id,ok,error) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                ok(JSON.parse(this.responseText));          
            }
            else if(this.status !== 200){
                error(this.status);
            }
        };
        xhttp.open("GET", "http://localhost:8080/api/participantes/"+id, true);
        xhttp.send();
    }

    atualizarParticipante(id,participante,ok,error) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                ok(JSON.parse(this.responseText));          
            }
            else if(this.status !== 200){
                error(this.status);
            }
        };
        xhttp.open("PUT", "http://localhost:8080/api/participantes/"+id, true);
        xhttp.setRequestHeader("Content-Type","application/json");
        xhttp.send(JSON.stringify(participante));
    }
}