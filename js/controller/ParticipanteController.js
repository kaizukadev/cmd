class ParticipanteController{
    constructor(){
        this.service = new ParticipanteHttpService();
        this.tabelaview = new TabelaParticipantesView(this,"main");
        this.formview = new FormParticipantesView(this,"main");

    }

    carregaFormulario( event){
        event.preventDefault();
        console.log(JSON.stringify(event));
        this.formview.montarForm();              
    }

    carregaFormularioComParticipante(id, event){
        event.preventDefault();             
        
        const self = this;
        const ok = function(participante){
            self.formview.montarForm(participante);
        }
        const erro = function(status){
            console.log(status);
        }

        this.service.buscarParticipante(id,ok,erro);   
    }
    

    salvarParticipante(event){
        event.preventDefault();
    
        let participante = this.formview.getDataParticipante();
        
        const self = this;

        this.service.enviarParticipante(participante, 
            () => {
                self.formview.limparFormulario();
                self.carregarParticipantes();
            },
            (status) => console.log(status)
        );
    }

    editarParticipante(id,event){
        event.preventDefault();
    
        let participante = this.formview.getDataParticipante();
        
        const self = this;

        this.service.atualizarParticipante(id,participante, 
            () => {
                self.formview.limparFormulario();
                self.carregarParticipantes();
            },
            (status) => console.log(status)
        );

    }

    carregarParticipantes(event) {   
        const self = this;
        const ok = function(listaParticipantes){
            self.tabelaview.montarTabela(listaParticipantes);              
        }
        const erro = function(status){
            console.log(status);
        }

        this.service.buscarParticipantes(ok,erro);
    }

    deletarParticipante(id, event){
        event.preventDefault();
            
        const self = this;

        this.service.deletarParticipante(id, 
            () => {
                self.carregarParticipantes();
            },
            (status) => console.log(status)
        );
    }
}