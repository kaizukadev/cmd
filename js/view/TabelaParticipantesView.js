class TabelaParticipantesView{
    constructor(controller, seletor){
        this.controller = controller;
        this.tabelaparticipantes = document.querySelector(seletor);
    }

    montarTabela(participantes){
        const self = this;

        var str=`
        <h2>Tabela de Participantes</h2>   
        <a id='novo' href='#'>INCLUIR</a>     
        <table>
            <tr>
                <th>Nome</th>
                <th>CPF</th> 
                <th>Data Nasc</th> 
                <th colspan="2">Ação</th>
            </tr>
    
        ${participantes.map(function(participante){
            return `
            <tr id=${participante.id}>
                <td>${participante.nome}</td>
                <td>${participante.cpf}</td>
                <td>${participante.dtNasc}</td>
                <td><a class="edit" href="#">Editar</a></td>
                <td><a class="delete" href="#">Deletar</a></td>
            </tr>
            `;
        }).join("")}
        
        </table>`;
    
        console.log(str);
        this.tabelaparticipantes.innerHTML = str;

        const linkNovo = document.querySelector("#novo");
        linkNovo.addEventListener("click",this.controller.carregaFormulario.bind(this.controller));
        
        const linksDelete = document.querySelectorAll(".delete");
        for(let linkDelete of linksDelete)
        {
            const id = linkDelete.parentNode.parentNode.id;
            linkDelete.addEventListener("click",this.controller.deletarParticipante.bind(this.controller,id));
        }

        const linksEdit = document.querySelectorAll(".edit");
        for(let linkEdit of linksEdit)
        {
            const id = linkEdit.parentNode.parentNode.id;
            linkEdit.addEventListener("click",this.controller.carregaFormularioComParticipante.bind(this.controller,id));
        }
    }
}