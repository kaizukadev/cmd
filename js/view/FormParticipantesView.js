class FormParticipantesView{
    constructor(controller, seletor){
        this.controller = controller;
        this.formparticipantes = document.querySelector(seletor);
    }

    montarForm(participante){
        if(!participante){
            participante = new Participante();
        }
        var str=`
        <h2>Cadastro de Participantes</h2>
        <form>
            <input type="hidden" id="idparticipante" value="${participante.id}" />
            <label for="nome">Nome:</label>    
            <input type="text" id="nome" value="${participante.nome ?participante.nome :''}"/> <br/>
            <label for="cpf">C.P.F.:</label>    
            <input type="text" id="cpf" value="${participante.cpf ?participante.cpf :''}"/> <br/>
            <label for="dtNasc">Dt. Nasc.:</label>    
            <input type="text" id="dtNasc" value="${participante.dtNasc ?participante.dtNasc :''}"/> <br/>
            <input type="submit" value="Salvar" />
            <input type="button" value="Cancelar" id="botaocancelar" />
        </form>`;
    
        this.formparticipantes.innerHTML = str;

        let elForm = document.querySelector("form");
        if(!participante.id){
            elForm.addEventListener("submit",this.controller.salvarParticipante.bind(this.controller));
        }
        else {
            elForm.addEventListener("submit",this.controller.editarParticipante.bind(this.controller, participante.id));
        }
        const botaocancelar = document.querySelector("#botaocancelar");
        botaocancelar.addEventListener("click",this.controller.carregarParticipantes.bind(this.controller));

    }

    limparFormulario(){
        document.querySelector("#idparticipante").value="";
        document.querySelector("#nome").value="";
        document.querySelector("#cpf").value="";
        document.querySelector("#dtNasc").value="";
    }

    getDataParticipante(){
        let participante = new Participante();
        if(!document.querySelector("#idparticipante").value)
            participante.id = document.querySelector("#idparticipante").value;
        participante.nome = document.querySelector("#nome").value;
        participante.cpf = document.querySelector("#cpf").value;
        participante.dtNasc = document.querySelector("#dtNasc").value;
        return participante;        
    }
}