class ParticipanteView {
    constructor(selForm, selTabela) {
        this.seletorFormulario = selForm;
        this.seletorTabela = selTabela;
    }

    montarTabela(participantes) {
        console.log(this.templateTabela(participantes));
        
        var tabela = document.querySelector(this.seletorTabela);
        tabela.innerHTML = this.templateTabela(participantes);
    }


    templateTabela(listaParticipantes) {
        return `<table border='1px'>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th> 
                        <th>Data Nasc</th> 
                    <tr>
                 </thead>
                 <tbody>
                    ${listaParticipantes.map(participante =>
                        `<tr>
                            <td>${participante.nome}</td>
                            <td>${participante.cpf}</td>
                            <td>${participante.dtNasc}</td>
                        </tr>
                        `).join('')}
                 </tbody>
            </table>`;
    }
}