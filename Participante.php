<?php
    class Participante {
        public $id;
        public $nome;
        public $cpf;
        public $dtNasc;

        function __construct($id, $nome, $cpf, $dtNasc){
            $this->id = $id;
            $this->nome = $nome;
            $this->cpf = $cpf;
            $this->dtNasc = $dtNasc;
        }
    }
?>