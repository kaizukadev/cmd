<?php
    include_once 'Evento.php';
	include_once 'PDOFactory.php';
    include_once 'Participante.php';
    include_once 'ParticipanteDAO.php';

    class EventoDAO
    {
        public function listar()
        {
		    $query = 'SELECT * FROM eventos';
    		$pdo = PDOFactory::getConexao();
	    	$comando = $pdo->prepare($query);
    		$comando->execute();
            $eventos=array();	
		    while($row = $comando->fetch(PDO::FETCH_OBJ)){
                $eventos[] = new Evento($row->id, $row->nome, $row->dtIni, $row->dtFim,
                                        $row->etapa01, $row->etapa02, $row->etapa03,
                                        ParticipanteDAO::buscarPorId($row->idCoord), 
                                        ParticipanteDAO::buscarPorId($row->idJur01), 
                                        ParticipanteDAO::buscarPorId($row->idJur02), 
                                        ParticipanteDAO::buscarPorId($row->idJur03), 
                                        $row->editar, $row->votar,
                                        $row->ts_new, $row->ts_upd);
            }
            return $eventos;
        }

        public function inserir(Evento $evt)
        {
            $qInserir = "INSERT INTO eventos (nome, dtIni, dtFim, etapa01, etapa02, etapa03, idCoord, idJur01, 
                         idJur02, idJur03, editar, votar) 
                         VALUES (:param01,:param02,:param03,:param04,:param05,:param06,:param07,:param08,:param09,
                         :param10,:param11,:param12)";            
            $pdo = PDOFactory::getConexao();
            $comando = $pdo->prepare($qInserir);
            $comando->bindParam(":param01",$evt->nome);
            $comando->bindParam(":param02",$evt->dtIni);
            $comando->bindParam(":param03",$evt->dtFim);
            $comando->bindParam(":param04",$evt->etapa01);
            $comando->bindParam(":param05",$evt->etapa02);
            $comando->bindParam(":param06",$evt->etapa03);
            $comando->bindParam(":param07",$evt->coordenador['id']);
            $comando->bindParam(":param08",$evt->jurado01['id']);
            $comando->bindParam(":param09",$evt->jurado02['id']);
            $comando->bindParam(":param10",$evt->jurado03['id']);
            $comando->bindParam(":param11",$evt->editar);
            $comando->bindParam(":param12",$evt->votar);
            $comando->execute();
            $evt->id = (int) $pdo->lastInsertId();
            return $evt;
        }

        public function deletar($id)
        {
            $qDeletar = "DELETE from usuarios WHERE id=:id";            
            $pdo = PDOFactory::getConexao();
            $comando = $pdo->prepare($qDeletar);
            $comando->bindParam(":id",$id);
            $comando->execute();
        }

        public function atualizar(Usuario $usuario)
        {
            $qAtualizar = "UPDATE usuarios SET username=:param1, password=:param2, id_part=:param3 WHERE id=:param0";            
            $pdo = PDOFactory::getConexao();
            $comando = $pdo->prepare($qAtualizar);
            $comando->bindParam(":param1",$usuario->username);
            $comando->bindParam(":param2",$usuario->password);
            $comando->bindParam(":param3",$usuario->participante['id']);
            $comando->bindParam(":param0",$usuario->id);
            $comando->execute();        
        }

        public function buscarPorId($id)
        {
 		    $query = "SELECT * FROM eventos WHERE id=:param0";		
            $pdo = PDOFactory::getConexao(); 
		    $comando = $pdo->prepare($query);
		    $comando->bindParam (":param0", $id);
		    $comando->execute();
		    $result = $comando->fetch(PDO::FETCH_OBJ);
            return new Evento($result->id, $result->nome, $result->dtIni, $result->dtFim,
                                $result->etapa01, $result->etapa02, $result->etapa03,
                                ParticipanteDAO::buscarPorId($result->idCoord), 
                                ParticipanteDAO::buscarPorId($result->idJur01), 
                                ParticipanteDAO::buscarPorId($result->idJur02), 
                                ParticipanteDAO::buscarPorId($result->idJur03), 
                                $result->editar, $result->votar,
                                $result->ts_new, $result->ts_upd);
        }
    }
?>