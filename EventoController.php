<?php

    // Classes principais
    include_once 'Evento.php';
    include_once 'EventoDAO.php';

    // Classe auxiliares
    include_once 'Participante.php';
    include_once 'ParticipanteDAO.php';


    class EventoController{     
        public function listar($request, $response,$args)
        {
            $dao = new EventoDAO;    
            $array_eventos = $dao->listar();        
            $response = $response->withJson($array_eventos);
            $response = $response->withHeader('Content-type', 'application/json');    
            return $response;
        }

        public function inserir( $request, $response, $args)
        {
            $var = $request->getParsedBody();
            $evt = new Evento(0, $var['nome'], $var['dtIni'], $var['dtFim'], $var['etapa01'], $var['etapa02'], 
                                $var['etapa03'], $var['coordenador'], $var['jurado01'], $var['jurado02'], 
                                $var['jurado03'], (int) $var['editar'], (int) $var['votar'], '', '');
            $dao = new EventoDAO;    
            $evt = $dao->inserir($evt);
            $response = $response->withJson($evt);
            $response = $response->withHeader('Content-type', 'application/json');    
            $response = $response->withStatus(201);
            return $response;
        }

        public function buscarPorId($request, $response, $args)
        {
            $id = (int) $args['id'];
            $dao = new EventoDAO;    
            $evt = $dao->buscarPorId($id);  
            $response = $response->withJson($evt);
            $response = $response->withHeader('Content-type', 'application/json');    
            return $response;
        }

        public function deletar($request, $response, $args)
        {
            $id = (int) $args['id'];
            $dao = new UsuarioDAO; 
            $usuario = $dao->buscarPorId($id);   
            $dao->deletar($id);
            $response = $response->withJson($usuario);
            $response = $response->withHeader('Content-type', 'application/json');    
            return $response;
        }

        public function atualizar($request, $response, $args)
        {
            $id = (int) $args['id'];
            $var = $request->getParsedBody();
            $usuario = new Usuario($id, $var['username'], $var['password'], $var['participante']);
            // $usuario = new Usuario($id, $var['username'], $var['password'], $var['id_part']);
            $dao = new UsuarioDAO;    
            $dao->atualizar($usuario);
            $response = $response->withJson($usuario);
            $response = $response->withHeader('Content-type', 'application/json');    
            return $response;        
        }
    }
?>