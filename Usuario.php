<?php
    class Usuario {
        public $id;
        public $username;
        public $password;
        public $participante;

        function __construct($id, $username, $password, $participante){
            $this->id = $id;
            $this->username = $username;
            $this->password = $password;
            $this->participante = $participante;
        }
    }
?>