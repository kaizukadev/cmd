<?php
    include_once 'Usuario.php';
	include_once 'PDOFactory.php';
    include_once 'Participante.php';
    include_once 'ParticipanteDAO.php';

    class UsuarioDAO
    {
        public function listar()
        {
		    $query = 'SELECT * FROM usuarios';
    		$pdo = PDOFactory::getConexao();
	    	$comando = $pdo->prepare($query);
    		$comando->execute();
            $usuarios=array();	
		    while($row = $comando->fetch(PDO::FETCH_OBJ)){
                $usuarios[] = new Usuario($row->id,$row->username,$row->password, ParticipanteDAO::buscarPorId($row->id_part));
            }
            return $usuarios;
        }

        public function inserir(Usuario $usuario)
        {
            $qInserir = "INSERT INTO usuarios (username,password,id_part) VALUES (:param1,:param2,:param3)";            
            $pdo = PDOFactory::getConexao();
            $comando = $pdo->prepare($qInserir);
            $comando->bindParam(":param1",$usuario->username);
            $comando->bindParam(":param2",$usuario->password);
            $comando->bindParam(":param3",$usuario->participante['id']);
            $comando->execute();
            $usuario->id = $pdo->lastInsertId();
            return $usuario;
        }

        public function deletar($id)
        {
            $qDeletar = "DELETE from usuarios WHERE id=:id";            
            $pdo = PDOFactory::getConexao();
            $comando = $pdo->prepare($qDeletar);
            $comando->bindParam(":id",$id);
            $comando->execute();
        }

        public function atualizar(Usuario $usuario)
        {
            $qAtualizar = "UPDATE usuarios SET username=:param1, password=:param2, id_part=:param3 WHERE id=:param0";            
            $pdo = PDOFactory::getConexao();
            $comando = $pdo->prepare($qAtualizar);
            $comando->bindParam(":param1",$usuario->username);
            $comando->bindParam(":param2",$usuario->password);
            $comando->bindParam(":param3",$usuario->participante['id']);
            $comando->bindParam(":param0",$usuario->id);
            $comando->execute();        
        }

        public function buscarPorId($id)
        {
 		    $query = "SELECT * FROM usuarios WHERE id=:param0";		
            $pdo = PDOFactory::getConexao(); 
		    $comando = $pdo->prepare($query);
		    $comando->bindParam (":param0", $id);
		    $comando->execute();
		    $result = $comando->fetch(PDO::FETCH_OBJ);
		    return new Usuario($result->id,$result->username,$result->password, ParticipanteDAO::buscarPorId($result->id_part));           
        }
    }
?>