<?php
    include_once 'Participante.php';
	include_once 'PDOFactory.php';

    class ParticipanteDAO
    {
        public function listar()
        {
		    $query = 'SELECT * FROM participantes';
    		$pdo = PDOFactory::getConexao();
	    	$comando = $pdo->prepare($query);
    		$comando->execute();
            $participantes=array();	
		    while($row = $comando->fetch(PDO::FETCH_OBJ)){
			    $participantes[] = new Participante($row->id,$row->nome,$row->cpf,$row->dtNasc);
            }
            return $participantes;
        }

        public function inserir(Participante $participante)
        {
            $qInserir = "INSERT INTO participantes (nome,cpf,dtNasc) VALUES (:nome,:cpf,:dtNasc)";            
            $pdo = PDOFactory::getConexao();
            $comando = $pdo->prepare($qInserir);
            $comando->bindParam(":nome",$participante->nome);
            $comando->bindParam(":cpf",$participante->cpf);
            $comando->bindParam(":dtNasc",$participante->dtNasc);
            $comando->execute();
            $participante->id = $pdo->lastInsertId();
            return $participante;
        }

        public function deletar($id)
        {
            $qDeletar = "DELETE from participantes WHERE id=:id";            
            $pdo = PDOFactory::getConexao();
            $comando = $pdo->prepare($qDeletar);
            $comando->bindParam(":id",$id);
            $comando->execute();
        }

        public function atualizar(Participante $participante)
        {
            $qAtualizar = "UPDATE participantes SET nome=:nome, cpf=:cpf, dtNasc=:dtNasc WHERE id=:id";            
            $pdo = PDOFactory::getConexao();
            $comando = $pdo->prepare($qAtualizar);
            $comando->bindParam(":nome",$participante->nome);
            $comando->bindParam(":cpf",$participante->cpf);
            $comando->bindParam(":dtNasc",$participante->dtNasc);
            $comando->bindParam(":id",$participante->id);
            $comando->execute();        
        }

        public function buscarPorId($id)
        {
 		    $query = 'SELECT * FROM participantes WHERE id=:id';		
            $pdo = PDOFactory::getConexao(); 
		    $comando = $pdo->prepare($query);
		    $comando->bindParam ('id', $id);
		    $comando->execute();
		    $result = $comando->fetch(PDO::FETCH_OBJ);
		    return new Participante($result->id,$result->nome,$result->cpf,$result->dtNasc);           
        }
    }
?>