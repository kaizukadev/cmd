-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.36-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.5.0.5325
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para cmd
CREATE DATABASE IF NOT EXISTS `cmd` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cmd`;

-- Copiando estrutura para tabela cmd.candidatos
CREATE TABLE IF NOT EXISTS `candidatos` (
  `idEvt` int(10) unsigned NOT NULL,
  `idPart` int(10) unsigned NOT NULL,
  `notaE01J01` decimal(3,1) unsigned NOT NULL,
  `notaE01J02` decimal(3,1) unsigned NOT NULL,
  `notaE01J03` decimal(3,1) unsigned NOT NULL,
  `notaE02J01` decimal(3,1) unsigned NOT NULL,
  `notaE02J02` decimal(3,1) unsigned NOT NULL,
  `notaE02J03` decimal(3,1) unsigned NOT NULL,
  `notaE03J01` decimal(3,1) unsigned NOT NULL,
  `notaE03J02` decimal(3,1) unsigned NOT NULL,
  `notaE03J03` decimal(3,1) unsigned NOT NULL,
  `ts_new` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ts_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idEvt`,`idPart`),
  KEY `FK_participantes` (`idPart`),
  CONSTRAINT `FK_eventos` FOREIGN KEY (`idEvt`) REFERENCES `eventos` (`id`),
  CONSTRAINT `FK_participantes` FOREIGN KEY (`idPart`) REFERENCES `participantes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela cmd.candidatos: ~0 rows (aproximadamente)
DELETE FROM `candidatos`;
/*!40000 ALTER TABLE `candidatos` DISABLE KEYS */;
INSERT INTO `candidatos` (`idEvt`, `idPart`, `notaE01J01`, `notaE01J02`, `notaE01J03`, `notaE02J01`, `notaE02J02`, `notaE02J03`, `notaE03J01`, `notaE03J02`, `notaE03J03`, `ts_new`, `ts_upd`) VALUES
	(1, 5, 9.3, 8.0, 9.0, 10.0, 9.8, 9.7, 9.9, 9.8, 9.8, '2018-11-07 23:33:06', '2018-11-07 23:33:09');
/*!40000 ALTER TABLE `candidatos` ENABLE KEYS */;

-- Copiando estrutura para tabela cmd.eventos
CREATE TABLE IF NOT EXISTS `eventos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `dtIni` date NOT NULL,
  `dtFim` date NOT NULL,
  `etapa01` varchar(20) NOT NULL DEFAULT 'AULA',
  `etapa02` varchar(20) NOT NULL DEFAULT 'REPERTORIO',
  `etapa03` varchar(20) NOT NULL DEFAULT 'IMPROVISACAO',
  `idCoord` int(10) unsigned NOT NULL DEFAULT '0',
  `idJur01` int(10) unsigned NOT NULL DEFAULT '0',
  `idJur02` int(10) unsigned NOT NULL DEFAULT '0',
  `idJur03` int(10) unsigned NOT NULL DEFAULT '0',
  `editar` bit(1) NOT NULL,
  `votar` bit(1) NOT NULL,
  `ts_new` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ts_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_Jurado_01` (`idJur01`),
  KEY `FK_Jurado_02` (`idJur02`),
  KEY `FK_Jurado_03` (`idJur03`),
  KEY `FK_Coordenador` (`idCoord`),
  CONSTRAINT `FK_Coordenador` FOREIGN KEY (`idCoord`) REFERENCES `participantes` (`id`),
  CONSTRAINT `FK_Jurado_01` FOREIGN KEY (`idJur01`) REFERENCES `participantes` (`id`),
  CONSTRAINT `FK_Jurado_02` FOREIGN KEY (`idJur02`) REFERENCES `participantes` (`id`),
  CONSTRAINT `FK_Jurado_03` FOREIGN KEY (`idJur03`) REFERENCES `participantes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela cmd.eventos: ~2 rows (aproximadamente)
DELETE FROM `eventos`;
/*!40000 ALTER TABLE `eventos` DISABLE KEYS */;
INSERT INTO `eventos` (`id`, `nome`, `dtIni`, `dtFim`, `etapa01`, `etapa02`, `etapa03`, `idCoord`, `idJur01`, `idJur02`, `idJur03`, `editar`, `votar`, `ts_new`, `ts_upd`) VALUES
	(1, 'SELEÇÃO BAILARINOS CIA MUNICIPAL DE DANÇA - 2019', '2018-11-25', '2018-11-27', 'AULA', 'REPERTORIO', 'IMPROVISACAO', 1, 4, 2, 3, b'1', b'0', '2018-11-05 22:30:14', '2018-11-24 20:03:35'),
	(2, 'SELEÇÃO BAILARINOS - TESTE 2', '2018-11-25', '2018-11-27', 'AULA2', 'REPERTORIO2', 'IMPROVISACAO2', 4, 3, 1, 2, b'1', b'1', '2018-11-24 23:16:31', '2018-11-24 23:16:31'),
	(3, 'SELEÇÃO BAILARINOS - TESTE 3', '2019-02-12', '2019-02-13', 'AULA3', 'REPERTORIO3', 'IMPROVISACAO3', 1, 5, 2, 3, b'1', b'1', '2018-11-25 20:37:12', '2018-11-25 20:37:12'),
	(4, 'SELEÇÃO BAILARINOS - TESTE 4', '2019-02-22', '2019-02-23', 'AULA4', 'REPERTORIO4', 'IMPROVISACAO4', 5, 4, 3, 2, b'1', b'1', '2018-11-25 20:47:26', '2018-11-25 20:47:26'),
	(5, 'SELEÇÃO BAILARINOS - TESTE 5', '2019-05-22', '2019-05-23', 'AULA5', 'REPERTORIO5', 'IMPROVISACAO5', 1, 2, 3, 4, b'1', b'1', '2018-11-25 21:16:50', '2018-11-25 21:16:50'),
	(6, 'SELEÇÃO BAILARINOS - TESTE 6', '2019-06-22', '2019-06-23', 'AULA6', 'REPERTORIO6', 'IMPROVISACAO6', 2, 1, 5, 2, b'1', b'1', '2018-11-25 21:24:58', '2018-11-25 21:24:58'),
	(7, 'SELEÇÃO BAILARINOS - TESTE 7', '2019-07-22', '2019-07-23', 'AULA7', 'REPERTORIO7', 'IMPROVISACAO7', 3, 5, 2, 4, b'1', b'1', '2018-11-25 21:41:47', '2018-11-25 21:41:47'),
	(8, 'SELEÇÃO BAILARINOS - TESTE 8', '2019-08-22', '2019-08-23', 'AULA8', 'REPERTORIO8', 'IMPROVISACAO8', 3, 5, 2, 4, b'1', b'1', '2018-11-25 21:45:46', '2018-11-25 21:45:46'),
	(9, 'SELEÇÃO BAILARINOS - TESTE 9', '2019-09-22', '2019-09-23', 'AULA9', 'REPERTORIO9', 'IMPROVISACAO9', 3, 5, 2, 4, b'1', b'1', '2018-11-25 21:50:39', '2018-11-25 21:50:39');
/*!40000 ALTER TABLE `eventos` ENABLE KEYS */;

-- Copiando estrutura para tabela cmd.participantes
CREATE TABLE IF NOT EXISTS `participantes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` char(50) NOT NULL,
  `cpf` char(11) NOT NULL,
  `dtNasc` date NOT NULL,
  `ts_new` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ts_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cpf` (`cpf`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela cmd.participantes: ~5 rows (aproximadamente)
DELETE FROM `participantes`;
/*!40000 ALTER TABLE `participantes` DISABLE KEYS */;
INSERT INTO `participantes` (`id`, `nome`, `cpf`, `dtNasc`, `ts_new`, `ts_upd`) VALUES
	(1, 'Sérgio Ferreira', '50355560089', '1965-02-12', '2018-11-04 21:05:24', '2018-11-04 21:05:25'),
	(2, 'Pedro Marques de Souza', '12345678901', '2000-03-10', '2018-11-04 21:07:42', '2018-11-04 21:08:41'),
	(3, 'Johnny Masterson', '12332112345', '1970-11-15', '2018-11-05 21:12:14', '2018-11-05 21:12:14'),
	(4, 'Márcia Rodrigues Alves', '00233366698', '1999-11-07', '2018-11-07 23:17:36', '2018-11-07 23:17:38'),
	(5, 'Guilhermina Flores', '12365498712', '1995-07-25', '2018-11-07 23:31:35', '2018-11-07 23:31:39');
/*!40000 ALTER TABLE `participantes` ENABLE KEYS */;

-- Copiando estrutura para tabela cmd.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(50) NOT NULL,
  `password` char(25) NOT NULL,
  `id_part` int(10) unsigned NOT NULL,
  `ts_new` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ts_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `id_part` (`id_part`),
  CONSTRAINT `FK_usuarios_participantes` FOREIGN KEY (`id_part`) REFERENCES `participantes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela cmd.usuarios: ~2 rows (aproximadamente)
DELETE FROM `usuarios`;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `username`, `password`, `id_part`, `ts_new`, `ts_upd`) VALUES
	(1, 'master', 'master', 1, '2018-11-04 20:34:16', '2018-11-04 21:06:54'),
	(2, 'joao', 'qwerty2018', 2, '2018-11-04 20:36:18', '2018-11-04 21:07:03'),
	(3, 'atalaia@gmail.com', '12345', 5, '2018-11-25 21:08:13', '2018-11-25 21:08:13');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
