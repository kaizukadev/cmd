<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once 'UsuarioController.php';
include_once 'ParticipanteController.php';
include_once 'EventoController.php';
require './vendor/autoload.php';

$app = new \Slim\App;

$app->group('/api/usuarios', function(){
    $this->get('','UsuarioController:listar');
    $this->post('','UsuarioController:inserir');
    $this->delete('/{id:[0-9]+}','UsuarioController:deletar');
    $this->get('/{id:[0-9]+}','UsuarioController:buscarPorId');
    $this->put('/{id:[0-9]+}','UsuarioController:atualizar');
});

$app->group('/api/participantes', function(){
    $this->get('','ParticipanteController:listar');
    $this->post('','ParticipanteController:inserir');
    $this->delete('/{id:[0-9]+}','ParticipanteController:deletar');
    $this->get('/{id:[0-9]+}','ParticipanteController:buscarPorId');
    $this->put('/{id:[0-9]+}','ParticipanteController:atualizar');
});

$app->group('/api/eventos', function(){
    $this->get('','EventoController:listar');
    $this->post('','EventoController:inserir');
    $this->delete('/{id:[0-9]+}','EventoController:deletar');
    $this->get('/{id:[0-9]+}','EventoController:buscarPorId');
    $this->put('/{id:[0-9]+}','EventoController:atualizar');
});

$app->run();
?>