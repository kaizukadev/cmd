<?php

    include_once 'Participante.php';
    include_once 'ParticipanteDAO.php';

    class ParticipanteController{     
        public function listar($request, $response,$args)
        {
            $dao = new ParticipanteDAO;    
            $array_participantes = $dao->listar();        
            $response = $response->withJson($array_participantes);
            $response = $response->withHeader('Content-type', 'application/json');    
            return $response;
        }

        public function inserir( $request, $response, $args)
        {
            $var = $request->getParsedBody();
            $participante = new Participante(0, $var['nome'], $var['cpf'], $var['dtNasc']);
            $dao = new ParticipanteDAO;    
            $participante = $dao->inserir($participante);
            $response = $response->withJson($participante);
            $response = $response->withHeader('Content-type', 'application/json');    
            $response = $response->withStatus(201);
            return $response;
        }

        public function buscarPorId($request, $response, $args)
        {
            $id = (int) $args['id'];
            $dao = new ParticipanteDAO;    
            $participante = $dao->buscarPorId($id);  
            $response = $response->withJson($participante);
            $response = $response->withHeader('Content-type', 'application/json');    
            return $response;
        }

        public function deletar($request, $response, $args)
        {
            $id = (int) $args['id'];
            $dao = new ParticipanteDAO; 
            $participante = $dao->buscarPorId($id);   
            $dao->deletar($id);
            $response = $response->withJson($participante);
            $response = $response->withHeader('Content-type', 'application/json');    
            return $response;
        }

        public function atualizar($request, $response, $args)
        {
            $id = (int) $args['id'];
            $var = $request->getParsedBody();
            $participante = new Participante($id, $var['nome'], $var['cpf'], $var['dtNasc']);
            $dao = new ParticipanteDAO;    
            $dao->atualizar($participante);
            $response = $response->withJson($participante);
            $response = $response->withHeader('Content-type', 'application/json');    
            return $response;        
        }
    }
?>